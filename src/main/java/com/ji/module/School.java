package com.ji.module;

import java.util.List;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/3/7
 * Version:1.1.0
 */
public class School {

    private String name;

    private String address;

    private List<Person> students;

    private String p;

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getStudents() {
        return students;
    }

    public void setStudents(List<Person> students) {
        this.students = students;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "School{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", students=" + students +
                '}';
    }
}
