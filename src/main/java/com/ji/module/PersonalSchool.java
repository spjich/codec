package com.ji.module;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/3/7
 * Version:1.1.0
 */
public class PersonalSchool extends School {
    private String personalName;


    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }
}
